<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("Shaun");

echo "Sheep Name : ". $sheep->name. "<br>"; // "shaun"
echo "Sheep legs: ".$sheep->legs. "<br>"; // 4
echo "cold blooded: ".$sheep->cold_blooded. "<br><br>"; //No

$frog = new Frog("buduk");

echo "Frog Name: ". $frog->name."<br>";
echo "Frog legs: ".$frog->legs. "<br>"; 
echo "cold blooded: ".$frog->cold_blooded. "<br>"; 
echo "Jump: ".$frog->jump . "<br><br>"; 

$sunwukong = new Ape("Kera Sakti");

echo "Ape Name: ". $sunwukong->name."<br>";
echo "Ape legs: ".$sunwukong->legs. "<br>"; 
echo "cold blooded: ".$sunwukong->cold_blooded. "<br>"; 
echo "Yell: ".$sunwukong->yell . "<br>"; 


?>